import React, { useEffect, useState } from 'react';
import { fetchTodos } from '../api';
import Header from './parts/header/Header';
import Todos from './parts/todos/Todos';
import Footer from './parts/footer/Footer';

const Main = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const getTodos = async () => {
      const response = await fetchTodos();
      setTodos(response.data);
    };
    getTodos();
  }, []);

  return (
    <>
      <Header />
      <Todos todos={todos} />
      <Footer />
    </>
  );
};

export default Main;

import React from 'react';
import Navbar from 'react-bootstrap/Navbar';

import './Footer.scss';

const Footer = () => {
  return (
    <>
      <Navbar expand="lg" className="footer" fixed="bottom">
        <Navbar.Brand className="mx-auto">
          <span className="copyright"> Copyright &copy; 2021</span>
        </Navbar.Brand>
      </Navbar>
    </>
  );
};

export default Footer;

import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import './Header.scss';

const Header = () => {
  return (
    <>
      <Navbar className="navbar" expand="lg">
        <Navbar.Brand>
          <span className="brand">Todos</span>{' '}
        </Navbar.Brand>

        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          ></Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Header;

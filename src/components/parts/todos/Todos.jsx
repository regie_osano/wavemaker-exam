import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import FormControl from 'react-bootstrap/FormControl';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import InputGroup from 'react-bootstrap/InputGroup';

import './Todos.scss';

let input = '';

const Todos = ({ todos }) => {
  let lastFilteredData = todos;
  const [taskTodos, setTaskTodos] = useState([]);

  const handleSearchInput = (e) => {
    input = e.target.value;
    if (input === '') {
      setTaskTodos(todos);
    } else {
      const filteredData = todos.filter((t) => t.userId === Number(input));
      setTaskTodos(filteredData);
    }
  };

  const handleSelectStatus = (eventKey) => {
    const searchStatus = eventKey === 'done' ? true : false;
    if (input === '') {
      const filteredDataByStatus = todos.filter(
        (t) => t.completed === searchStatus
      );
      setTaskTodos(filteredDataByStatus);
    } else {
      const filteredDataByStatus = lastFilteredData.filter(
        (t) => t.completed === searchStatus && t.userId === Number(input)
      );
      setTaskTodos(filteredDataByStatus);
      lastFilteredData = filteredDataByStatus;
    }
  };

  useEffect(() => {
    setTaskTodos(todos);
  }, [todos]);

  const message = (
    <tr>
      <td>No Records!</td>
    </tr>
  );

  return (
    <>
      <div className="container">
        <Container>
          <div className="search-box">
            <InputGroup className="mb-3">
              <FormControl placeholder="Search" onChange={handleSearchInput} />

              <DropdownButton
                variant="outline-secondary"
                title="Status"
                onSelect={handleSelectStatus}
                id="statusDropdown"
                align="end"
              >
                <Dropdown.Item eventKey={'done'}>Done</Dropdown.Item>
                <Dropdown.Item eventKey={'in-progress'}>
                  In-progress
                </Dropdown.Item>
              </DropdownButton>
            </InputGroup>
          </div>
          <div className="table">
            <Table bordered hover>
              <thead>
                <tr>
                  <th className="sticky-header">No.</th>
                  <th className="sticky-header h-align">User ID</th>
                  <th className="sticky-header h-align">Task</th>
                  <th className="sticky-header h-align">Completed</th>
                </tr>
              </thead>
              <tbody>
                {taskTodos.length === 0
                  ? message
                  : taskTodos.map((todo, index) => {
                      return (
                        <tr key={todo.id}>
                          <td className="no">{index + 1}.</td>
                          <td className="userId">{todo.userId}</td>
                          <td className="task">{todo.title}</td>
                          <td className="completed">
                            {todo.completed ? <>Done</> : <>In-Progress</>}
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </Table>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Todos;
